/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author hp
 */
public class Ventana extends javax.swing.JFrame {

    /**
     * Creates new form Interfaz
     */
    BufferedReader reader;
    HiloMensajes hm = new HiloMensajes();
    String buffer = "";
    public Ventana() {
        initComponents();
        setLocationRelativeTo(null);
        reader = new BufferedReader(new InputStreamReader(System.in));        
        hm.start();
    }

    public Ventana(BufferedReader reader, JTextArea areaTextoChat, JTextArea areaTextoConectados, JButton botonEnviar, JButton botonIngresar, JLabel etiquetaID, JScrollPane jScrollPane1, JScrollPane jScrollPane2, JPanel panel, JTextField tfTexto, JTextField tfUsuario) throws HeadlessException {
        this.reader = reader;
        this.areaTextoChat = areaTextoChat;
        this.areaTextoConectados = areaTextoConectados;
        this.botonEnviar = botonEnviar;
        this.botonIngresar = botonIngresar;
        this.etiquetaID = etiquetaID;
        this.jScrollPane1 = jScrollPane1;
        this.jScrollPane2 = jScrollPane2;
        this.panel = panel;
        this.tfTexto = tfTexto;
        this.tfUsuario = tfUsuario;
    }
    
    

    /**
     
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        botonIngresar = new javax.swing.JButton();
        tfUsuario = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaTextoChat = new javax.swing.JTextArea();
        botonEnviar = new javax.swing.JButton();
        tfTexto = new javax.swing.JTextField();
        etiquetaID = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        areaTextoConectados = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Proyecto Chat");

        panel.setBackground(new java.awt.Color(153, 153, 255));
        panel.setBorder(javax.swing.BorderFactory.createLineBorder(getBackground(), 10));
        panel.setLayout(null);

        botonIngresar.setText("Ingresar");
        botonIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIngresarActionPerformed(evt);
            }
        });
        panel.add(botonIngresar);
        botonIngresar.setBounds(465, 45, 135, 30);
        panel.add(tfUsuario);
        tfUsuario.setBounds(225, 45, 225, 30);

        areaTextoChat.setEditable(false);
        areaTextoChat.setColumns(20);
        areaTextoChat.setRows(5);
        areaTextoChat.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        areaTextoChat.setEnabled(false);
        jScrollPane1.setViewportView(areaTextoChat);

        panel.add(jScrollPane1);
        jScrollPane1.setBounds(30, 90, 570, 360);

        botonEnviar.setText("Enviar");
        botonEnviar.setEnabled(false);
        botonEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEnviarActionPerformed(evt);
            }
        });
        panel.add(botonEnviar);
        botonEnviar.setBounds(465, 470, 135, 40);

        tfTexto.setEnabled(false);
        panel.add(tfTexto);
        tfTexto.setBounds(30, 470, 420, 40);

        etiquetaID.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        etiquetaID.setText("ID DEL USUARIO");
        panel.add(etiquetaID);
        etiquetaID.setBounds(45, 45, 180, 30);

        areaTextoConectados.setEditable(false);
        areaTextoConectados.setColumns(20);
        areaTextoConectados.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        areaTextoConectados.setRows(5);
        areaTextoConectados.setText("          CONECTADOS");
        areaTextoConectados.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        areaTextoConectados.setEnabled(false);
        jScrollPane2.setViewportView(areaTextoConectados);

        panel.add(jScrollPane2);
        jScrollPane2.setBounds(615, 30, 330, 480);

        getContentPane().add(panel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIngresarActionPerformed
        chatServer server = new chatServer();
        if (tfUsuario.getText().equals(" ")) {
            JOptionPane.showMessageDialog(null, "Ingresa un nombre por favor");
        }else{
                           
                buffer = tfUsuario.getText();      
                
                if (buffer!=null){                    
                    hm.enviar(buffer);
                    //areaTextoConectados.setText(areaTextoConectados.getText() + "\n" + server.getUsuarios()+"\n");
                    this.botonEnviar.setEnabled(true);               
                    this.tfTexto.setEnabled(true);
                    this.tfUsuario.setEnabled(false);
                    this.areaTextoChat.setEnabled(true);
                    this.botonEnviar.setEnabled(true);
                }                                
           
        }
    }//GEN-LAST:event_botonIngresarActionPerformed

    private void botonEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEnviarActionPerformed
        buffer = tfTexto.getText();
        tfTexto.setText("");
        if (buffer != null) {
            hm.enviar(buffer);
            areaTextoChat.setText(areaTextoChat.getText() + " \n" + " > " + buffer + "\n");
            if (buffer.equals("salir")) {
                hm.cerrar();
                System.exit(0);
            }
        }
    }//GEN-LAST:event_botonEnviarActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaTextoChat;
    private javax.swing.JTextArea areaTextoConectados;
    private javax.swing.JButton botonEnviar;
    private javax.swing.JButton botonIngresar;
    private javax.swing.JLabel etiquetaID;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel panel;
    private javax.swing.JTextField tfTexto;
    private javax.swing.JTextField tfUsuario;
    // End of variables declaration//GEN-END:variables

    
    class HiloMensajes extends Thread {
    Socket clientSocket;
    PrintWriter out;
    BufferedReader in;              
    String buffer;
    SimpleDateFormat formatterMDY;
    boolean salir = false;
    
    public void run(){        
        String fechaHora;
        //formatterMDY = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            
        try {
            clientSocket = new Socket("192.168.0.15",3333);

            out =
                    new PrintWriter(clientSocket.getOutputStream(), true);
            
            in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));  
            
            while(!this.salir){                
                buffer = in.readLine();
                
                if (buffer!=null){
                    //fechaHora = formatterMDY.format(Calendar.getInstance().getTime());
                  areaTextoChat.setText(areaTextoChat.getText()+" \n"+" < "+buffer);
                 //  System.out.println(fechaHora+ " < "+buffer);
                  //  System.out.print("> ");
                }                
            }                        
        } catch (IOException ex) {
            Logger.getLogger(HiloMensajes.class.getName()).log(Level.SEVERE, null, ex);
        }                           
    }

    void enviar(String mensaje) {
        out.println(mensaje);
    }

    void cerrar() {
        try {
            clientSocket.close();
            in.close();
            out.close();
            this.salir = true;
        } catch (IOException ex) {
            Logger.getLogger(HiloMensajes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 }
    /**
     * @return the areaTextoChat
     */
    public javax.swing.JTextArea getAreaTextoChat() {
        return areaTextoChat;
    }

    /**
     * @param areaTextoChat the areaTextoChat to set
     */
    public void setAreaTextoChat(javax.swing.JTextArea areaTextoChat) {
        this.areaTextoChat = areaTextoChat;
    }

    /**
     * @return the areaTextoConectados
     */
    public javax.swing.JTextArea getAreaTextoConectados() {
        return areaTextoConectados;
    }

    /**
     * @param areaTextoConectados the areaTextoConectados to set
     */
    public void setAreaTextoConectados(javax.swing.JTextArea areaTextoConectados) {
        this.areaTextoConectados = areaTextoConectados;
    }

    /**
     * @return the botonEnviar
     */
    public javax.swing.JButton getBotonEnviar() {
        return botonEnviar;
    }

    /**
     * @param botonEnviar the botonEnviar to set
     */
    public void setBotonEnviar(javax.swing.JButton botonEnviar) {
        this.botonEnviar = botonEnviar;
    }

    /**
     * @return the botonIngresar
     */
    public javax.swing.JButton getBotonIngresar() {
        return botonIngresar;
    }

    /**
     * @param botonIngresar the botonIngresar to set
     */
    public void setBotonIngresar(javax.swing.JButton botonIngresar) {
        this.botonIngresar = botonIngresar;
    }

    /**
     * @return the etiquetaID
     */
    public javax.swing.JLabel getEtiquetaID() {
        return etiquetaID;
    }

    /**
     * @param etiquetaID the etiquetaID to set
     */
    public void setEtiquetaID(javax.swing.JLabel etiquetaID) {
        this.etiquetaID = etiquetaID;
    }

    /**
     * @return the jScrollPane1
     */
    public javax.swing.JScrollPane getjScrollPane1() {
        return jScrollPane1;
    }

    /**
     * @param jScrollPane1 the jScrollPane1 to set
     */
    public void setjScrollPane1(javax.swing.JScrollPane jScrollPane1) {
        this.jScrollPane1 = jScrollPane1;
    }

    /**
     * @return the jScrollPane2
     */
    public javax.swing.JScrollPane getjScrollPane2() {
        return jScrollPane2;
    }

    /**
     * @param jScrollPane2 the jScrollPane2 to set
     */
    public void setjScrollPane2(javax.swing.JScrollPane jScrollPane2) {
        this.jScrollPane2 = jScrollPane2;
    }

    /**
     * @return the panel
     */
    public javax.swing.JPanel getPanel() {
        return panel;
    }

    /**
     * @param panel the panel to set
     */
    public void setPanel(javax.swing.JPanel panel) {
        this.panel = panel;
    }

    /**
     * @return the tfTexto
     */
    public javax.swing.JTextField getTfTexto() {
        return tfTexto;
    }

    /**
     * @param tfTexto the tfTexto to set
     */
    public void setTfTexto(javax.swing.JTextField tfTexto) {
        this.tfTexto = tfTexto;
    }

    /**
     * @return the tfUsuario
     */
    public javax.swing.JTextField getTfUsuario() {
        return tfUsuario;
    }

    /**
     * @param tfUsuario the tfUsuario to set
     */
    public void setTfUsuario(javax.swing.JTextField tfUsuario) {
        this.tfUsuario = tfUsuario;
    }
}

