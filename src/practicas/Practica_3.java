
package practicas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Practica_3 {
    JLabel etiqueta1;
    JLabel etiqueta2;
    JTextField campo1;
    JButton btnGenerar;
    JComboBox despegable1;
    
    public static void main(String[] args) {
        Practica_3 p1 = new Practica_3();
        p1.run();
    }
    
    public void run(){
        
        JFrame ventana = new JFrame("PRACTICA 3");
        ventana.setSize(450, 200);
        ventana.setLayout(null);
        ventana.setLocationRelativeTo(null);
        
        etiqueta1 = new JLabel("Escribe el titulo de una pelicula");
        etiqueta1.setBounds(50, 40, 190, 30);
        
        campo1 = new JTextField();
        campo1.setBounds(75, 70, 150, 25);
        
        btnGenerar = new JButton("Añadir");
        btnGenerar.setBounds(80, 105, 80, 30);
        
        etiqueta2 = new JLabel("Peliculas");
        etiqueta2.setBounds(285, 40, 100, 30);
        
        despegable1 = new JComboBox();
        despegable1.setBounds(260, 70, 100, 25); 
        
        btnGenerar.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                  String pelicula = campo1.getText();
                  boolean chismoso=true;
                  int x=0;
                  
                if (despegable1.getItemCount()==0) {
                    despegable1.addItem(pelicula);
                    despegable1.setSelectedItem(pelicula);
                    campo1.setText("");
                }else{
                    while (chismoso == true && x < despegable1.getItemCount()) {
                    if (pelicula.equalsIgnoreCase((String) despegable1.getItemAt(x))) {
                            chismoso = false;
                        }else{
                            chismoso = true;
                        }
                        x++;
                    }
                    if (chismoso == true) {
                        despegable1.addItem(pelicula);
                        despegable1.setSelectedItem(pelicula);
                        campo1.setText("");
                    }else{
                        JOptionPane.showMessageDialog(null, "Ya existe la pelicula");
                        campo1.setText("");
                    }  
                }         
            }
        });
        
        ventana.getContentPane().add(etiqueta1);
        ventana.getContentPane().add(campo1);
        ventana.getContentPane().add(btnGenerar);
        ventana.getContentPane().add(etiqueta2);
        ventana.getContentPane().add(despegable1);
        
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}



