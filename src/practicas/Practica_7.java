
package Practicas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.Arrays;
import java.util.Enumeration;
import javax.swing.ButtonGroup;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Practica_7 {
    JLabel etiqueta1, etiqueta2, etiqueta3,indicador;
    JRadioButton opcion1,opcion2,opcion3;
    JCheckBox opcion4, opcion5, opcion6;
    ButtonGroup grupo1;
    JSlider deslizador;
    JButton btnGenerar;
    JPanel panel;
    FileInputStream entrada;
    FileOutputStream salida;
    public static void main(String[] args) {
        Practica_7 app = new Practica_7();
        app.run();
    }
    public void run(){
        JFrame ventana = new JFrame("PRACTICA 7");//Creamos la ventana con el titulo PRACTICA 7
        ventana.setSize(280, 500);//Establecemos el tamaño de la ventana
        ventana.setLocationRelativeTo(null);//Hacemos que la ventana este en el centro de la pantalla
        ventana.setLayout(null);//Pegamos la plantilla a la ventana
        
        grupo1 = new ButtonGroup();
        
        etiqueta1 = new JLabel("Elige un sistema operativo");//Crea la etiqueta1 con un titulo
        etiqueta1.setBounds(20, 30, 150, 20);//Establecemos la ubicacion de la etiqueta 1 en la ventana
        
        opcion1 = new JRadioButton("Windows");//Creamos el radioButton con el titulo windows
        opcion1.setBounds(16, 65, 80, 20);//Establecemos la ubicacion de boton de opcion circular 
        
        opcion2 = new JRadioButton("Linux");//Creamos el radioButton con el tirulo Linux
        opcion2.setBounds(16, 90, 80, 20);//Establecemos la ubicacion del boton de opcion circular
        
        opcion3 = new JRadioButton("Mac");//Creamos el radioButton con el tirulo Linux
        opcion3.setBounds(16, 115, 80, 20);//Establecemos la ubicacion del boton de opcion circular
        
        grupo1.add(opcion1);//Añadimos el boton al grupo de boton 1
        grupo1.add(opcion2);//Añadimos el boton al grupo de boton 2
        grupo1.add(opcion3);//Añadimos el boton al grupo de boton 3
        
        etiqueta2 = new JLabel("Elige tu especialidad");//Creamos la etiqueta con un titulo
        etiqueta2.setBounds(20, 160, 150, 20);//Establecemos la ubicacion de la etiqueta 2
        
        opcion4 = new JCheckBox("Programacion");//Creamos el boton casillero 1 con un titulo
        opcion4.setBounds(22, 190, 120, 20);//Establecemos la ubicacion del boton casillero 1
        
        opcion5 = new JCheckBox("Diseño grafico");//Creamos el boton casillero 2 con un titulo
        opcion5.setBounds(22, 220, 120, 20);//Establecemos la ubicacion del boton casillero 2
        
        opcion6 = new JCheckBox("Administracion");//Creamos el boton casillero 3 con un titulo
        opcion6.setBounds(22, 250, 120, 20);//Establecemos la ubicacion del boton casillero 3
        
        etiqueta3 = new JLabel("Horas que dedicas en el ordenador");//Creamos la etiqueta 3 con un titulo largo :V
        etiqueta3.setBounds(20, 295, 200, 20);//Establecemos la ubicacion de la etiqueta
        
        deslizador = new JSlider();//Creamos un deslizador
        deslizador.setBounds(40, 340, 200, 20);//Establecemos la ubicacion del deslizador
        deslizador.setMaximum(24);//Establecemos cual es numero limite del deslizador
        deslizador.setMinimum(0);//Establecemos cual es el numero minimo del deslizador
        deslizador.setMinorTickSpacing(1);//Cada cuanto se va a partir el deslizador
        deslizador.setValue(12);//Cuando inicie el programa en que numero se mostrara al comienzo
        
        indicador = new JLabel(deslizador.getValue()+"");//Creamos una etiqueta llamada indicador y nos servira para saber que numero tiene el deslizador
        indicador.setBounds(20, 340, 20, 20);
        
        btnGenerar = new JButton("Generar");//Creamos el boton generar con un titulo
        btnGenerar.setBounds(90, 400, 80, 20);//Establecemos la ubicacion del boton
        
        
        deslizador.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                int numero = deslizador.getValue();
                indicador.setText(numero+"");
            }
        });
       
        btnGenerar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                JRadioButton boton = new JRadioButton();
                boton = botonSeleccionado(opcion1, opcion2, opcion3);
                String casillas = casillasSeleccionadas(opcion4, opcion5, opcion6);
                String contenido = "Tu sistema preferido es "+boton.getText()+", tus especialidades son \n"+casillas+
                        "y el numero de horas dedicadas \nal ordenador son "+indicador.getText();
                JOptionPane.showMessageDialog(null, contenido);
                
                try {
                    guardaArchivo2(contenido);
                    //File archivo = new File("C:\\Users\\alber\\Documents\\NetBeansProjects\\TAP78-AD1\\src\\Practicas\\miarchivo.txt");
                } catch (IOException ex) {
                    Logger.getLogger(Practica_7.class.getName()).log(Level.SEVERE, null, ex);
                }

                 
                
            }
            
        });
        
        
        ventana.getContentPane().add(etiqueta1);//Añadimos la etiqueta 1 a la plantilla -> ventana     
        ventana.getContentPane().add(opcion1);//Añadimos el boton circular de opcion 1 a la plantilla -> ventana
        ventana.getContentPane().add(opcion2);//Añadimos el boton circular de opcion 2 a la plantilla -> ventana
        ventana.getContentPane().add(opcion3);//Añadimos el boton circular de opcion 3 a la plantilla -> ventana
        ventana.getContentPane().add(etiqueta2);//Añadimos la etiqueta 2 a la plantilla -> ventana
        ventana.getContentPane().add(opcion4);//Añadimos el boton casillero 1 a la plantilla -> ventana
        ventana.getContentPane().add(opcion5);//Añadimos el boton casillero 2 a la plantilla -> ventana
        ventana.getContentPane().add(opcion6);//Añadimos el boton casillero 3 a la plantilla -> ventana
        ventana.getContentPane().add(etiqueta3);//Añadimos la etiqueta 3 a la plantilla -> ventana
        ventana.getContentPane().add(deslizador);//Añadimos el deslizador  a la plantilla -> ventana
        ventana.getContentPane().add(indicador);//Añadimos el indicador  a la plantilla -> ventana
        ventana.getContentPane().add(btnGenerar);//Añadimos el boton generar  a la plantilla -> ventana
        ventana.setVisible(true);//Hacemos visible la ventana
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //Cuando cierre la ventana detiene la ejecucion
    }
    public JRadioButton botonSeleccionado(JRadioButton boton1,JRadioButton boton2,JRadioButton boton3){
        //Este metodo nos ayuda a saber que boton esta seleccionado y nos retorna el boton.
        if (boton1.isSelected()==true) {
            return boton1;
        }else if (boton2.isSelected()==true) {
            return boton2;
        }else if (boton3.isSelected()==true) {
            return boton3;
        }
        return null;
        
    }
    
    public String casillasSeleccionadas(JCheckBox boton1,JCheckBox boton2,JCheckBox boton3){
        //Este metodo nos ayuda a saber que casillas estan seleccionadas y aparte te retorna un String con las epecialidades concatenadas
        
        String oracion=", ";
        if (boton1.isSelected()==true) {
            oracion = boton1.getText()+oracion;
        }
        if (boton2.isSelected() == true) {
            oracion = oracion + boton2.getText();
        }
        if (boton3.isSelected() == true) {
            oracion = oracion + ", "+boton3.getText();
        }
        return oracion;
    }
    
    public void guardarArchivo(File archivo, String contenido){
        //Este metodo crea un archivo y lo guarda en forma de texto
        String respuesta = null;
        //BUSCAR UNICODE
        try {
            salida = new FileOutputStream(archivo);
            byte[] bytesTxt = contenido.getBytes();
            salida.write(bytesTxt);
        } catch (Exception e) {}
    }
    void guardaArchivo2(String mensaje) throws IOException{
        FileWriter archivo;
        try {
            archivo = new FileWriter("respuestas.txt");//Nos conviente usar este
            archivo.write(mensaje+"\n");
            archivo.close();
        } catch (IOException ex) {
            Logger.getLogger(Practica_7.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
