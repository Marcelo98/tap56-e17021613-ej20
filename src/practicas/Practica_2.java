
package practicas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.WindowConstants;

public class Practica_2 {
    
    JFrame ventana;
    JLabel etiqueta1;
    JLabel etiqueta2;
    JLabel etiqueta3;
    JSpinner spin1; 
    JSpinner spin2; 
    JTextField tfCampo;
    JButton btnGenerar; 
    
    public static void main(String[] args) {
        Practica_2 app = new Practica_2();
        app.run();
    }
    
    void run(){
        
        ventana = new JFrame("Practica 2");
        ventana.setLayout(null);
        ventana.setSize(280, 310);
        ventana.setLocationRelativeTo(null);
        
        etiqueta1 = new JLabel("Número1");
        etiqueta1.setBounds(30, 40, 80, 20);
        
        spin1 = new JSpinner();
        spin1.setValue(0);
        spin1.setBounds(150, 40, 80, 20);
        etiqueta2 = new JLabel("Número2");
        etiqueta2.setBounds(30, 100, 80, 20);
        
        spin2 = new JSpinner();
        spin2.setValue(0);
        spin2.setBounds(150, 100, 80, 20);
        
        etiqueta3 = new JLabel("Número generado");
        etiqueta3.setBounds(30, 160, 130, 20);
        
        tfCampo = new JTextField();
        tfCampo.setBounds(150, 160, 80, 20);
        tfCampo.setHorizontalAlignment(JTextField.CENTER);
        
        btnGenerar = new JButton("Generar");
        btnGenerar.setBounds(150, 210, 80, 20);
        
        btnGenerar.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                
                Random MiAleatorio = new Random(); 
                int numeroA;
                int numeroB;
                int numeroAleatorio = 0;
                
                numeroA = (int) spin1.getValue();
                numeroB = (int) spin2.getValue();
                
                if (numeroA > numeroB) {
                    numeroAleatorio = (MiAleatorio.nextInt(numeroA-numeroB+1)+numeroB);
                }else{
                    if(numeroA < numeroB){
                        numeroAleatorio = (MiAleatorio.nextInt(numeroB-numeroA+1)+numeroA);
                    }else{
                        numeroAleatorio = numeroA | numeroB;
                    }
                }
                tfCampo.setText(numeroAleatorio+"");
            }
            
        });
         
        ventana.getContentPane().add(spin1);
        ventana.getContentPane().add(etiqueta1);
        ventana.getContentPane().add(etiqueta2);
        ventana.getContentPane().add(etiqueta3);
        ventana.getContentPane().add(spin2);
        ventana.getContentPane().add(tfCampo);
        ventana.getContentPane().add(btnGenerar);
        
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
    
}


