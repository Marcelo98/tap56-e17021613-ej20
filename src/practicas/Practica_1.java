package practicas;

import com.sun.corba.se.impl.encoding.CodeSetConversion;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Practica_1 {
    JFrame ventana;
    JLabel lblNombre;
    JTextField tfNombre;
    JButton btnSaludar;
    
    public static void main(String[] args) {
        Practica_1 app = new Practica_1();
        app.run();
        
     }
    
   void run(){
       
       ventana = new JFrame("Practica 1");
       ventana.setLayout(new FlowLayout());
       ventana.setSize(300, 200);
       
       lblNombre = new JLabel("Escriba su nombre");
       tfNombre = new JTextField(20);
       btnSaludar = new JButton("Saludar");
       
       btnSaludar.addActionListener(new ActionListener() {
           
           @Override
           public void actionPerformed(ActionEvent e) {
               
               JOptionPane.showMessageDialog(null, "Hola "+tfNombre.getText(), "Mensaje", 1);
               
           }
       });
       
       ventana.add(lblNombre);
       ventana.add(tfNombre);
       ventana.add(btnSaludar);
       
       ventana.setVisible(true);
       ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//Cierra la ventana y termina el proceso.
       
   }
    
}




